module.exports = {
	apps : [{
		name      : 'bridges/unflaired',
		script    : 'build/unflaired.js',
		env: {
			// Either inject the values via environment variables or define them here
			TRANSPORT_BIND_ADDRESS: process.env.TRANSPORT_BIND_ADDRESS || "",
			BRIDGE_UNFLAIRED_SUBREDDIT: process.env.BRIDGE_UNFLAIRED_SUBREDDIT || "",
			BRIDGE_UNFLAIRED_CHANNEL: process.env.BRIDGE_UNFLAIRED_SUBREDDIT || ""
		}
	}]
};
